﻿using System;
using MongoDBToturial.Controllers;
using MongoDBToturial.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MongoDBToturial
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("System Begin ...");

            NoteRepository noteRepository = new NoteRepository();

            noteRepository.ListNotes();


            Note note = new Note();
            note.id = "1";
            note.Body = "今天星期一";

            noteRepository.AddNoteSync(note);

            noteRepository.ListNotes();
        }
    }
}
