﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using MongoDBToturial.Models;

namespace MongoDBToturial.Controllers
{
    public interface INoteRepository
    {
        Task<IEnumerable<Note>> GetAllNotes();
        Task<Note> GetNote(string id);

        Task AddNote(Note item);

        Task<bool> RemoveNote(string id);

        Task<bool> UpdateNote(string id, string body);

        Task<bool> UpdateNoteDocuments(string id, string body);

        Task<bool> RemoveAllNotes();
    }
}
