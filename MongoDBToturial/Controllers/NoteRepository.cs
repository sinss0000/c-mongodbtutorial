﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDBToturial.Models;

namespace MongoDBToturial.Controllers
{
    public class NoteRepository: INoteRepository
    {
        private readonly NoteContext _context = null;

        public NoteRepository()
        {
            _context = new NoteContext();
        }

        public void ListNotes()
        {
            Console.WriteLine("List Notes ...");
            IEnumerable<Note> notes = _context.Notes.Find(_ => true).ToList();
            foreach (Note note in notes) {
                Console.WriteLine($"Id: {note.id}, body: {note.Body}");
            }
        }

        public async Task<IEnumerable<Note>> GetAllNotes()
        {
            try
            {
                return await _context.Notes.Find(_ => true).ToListAsync();
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public async Task<Note> GetNote(string id)
        {
            try {
                ObjectId internalId = GetInternalId(id);
                return await _context.Notes.Find(note => note.id == id || note.internalId == internalId).FirstOrDefaultAsync();
            } catch (Exception e) {
                throw e;
            }
        }

        private ObjectId GetInternalId(string id) {
            ObjectId internalId;
            if (!ObjectId.TryParse(id, out internalId))
            {
                internalId = ObjectId.Empty;
            }

            return internalId;
        }

        public async Task AddNote(Note item)
        {
            try {
                await _context.Notes.InsertOneAsync(item);
            } catch (Exception e) {
                throw e;
            }
        }

        public void AddNoteSync(Note item)
        {
            try
            {
                _context.Notes.InsertOne(item);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public async Task<bool> UpdateNote(string id, string body)
        {
            var filter = Builders<Note>.Filter.Eq(s => s.id, id);
            var update = Builders<Note>.Update.Set(s => s.Body, body);
            try {
                UpdateResult actionResult = await _context.Notes.UpdateOneAsync(filter, update);
                return actionResult.IsAcknowledged && actionResult.ModifiedCount > 0;
            } catch (Exception e) {
                throw e;
            }
        }

        public async Task<bool> UpdateNoteDocuments(string id, string body)
        {
            throw new NotImplementedException();
        }

        public async Task<bool> RemoveAllNotes()
        {
            try {
                DeleteResult result = await _context.Notes.DeleteManyAsync(new BsonDocument());
                return result.IsAcknowledged && result.DeletedCount > 0;
            } catch (Exception e) {
                throw e;
            }
        }

        public async Task<bool> RemoveNote(string id)
        {
            try
            {
                DeleteResult actionResult = await _context.Notes.DeleteOneAsync(Builders<Note>.Filter.Eq("id", id));

                return actionResult.IsAcknowledged && actionResult.DeletedCount > 0;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}
