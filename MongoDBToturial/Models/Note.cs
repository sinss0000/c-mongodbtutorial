﻿using System;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace MongoDBToturial.Models
{
    public class Note
    {
        [BsonId]
        public ObjectId internalId { get; set; }
        public string id { get; set; }
        public string Body { get; set; } = String.Empty;
        public DateTime UpdateAt { get; set; } = DateTime.Now;
        public DateTime CreatedAt { get; set; } = DateTime.Now;
        public int userId { get; set; } = 0;

        public Note()
        {
        }
    }
}
