﻿using System;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Driver;

namespace MongoDBToturial.Models
{
    public class NoteContext
    {
        private string connection = "mongodb://leo.chang:sinss0000@localhost";
        private string dbName = "local";
        private IMongoDatabase _database = null;
        public NoteContext() {
            initialized();
        }

        private void initialized() {
            var client = new MongoClient(connection);
            if (client != null) {
                _database = client.GetDatabase(dbName, null);
            }
            Console.WriteLine("Database connected ...");
        }

        public IMongoCollection<Note> Notes {
            get {
                return _database.GetCollection<Note>("Note");
            }
        }

    }
}
